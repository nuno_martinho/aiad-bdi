package gui;

import jadex.bridge.IComponentIdentifier;
import jadex.bridge.IExternalAccess;
import jadex.bridge.service.types.cms.CreationInfo;
import jadex.bridge.service.types.cms.IComponentManagementService;
import jadex.commons.future.IFuture;
import jadex.commons.future.ThreadSuspendable;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JButton;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;
import javax.swing.JTextField;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;

import agents.DriverAgentBDI;
import agents.MapAgent;
import agents.WorldAgentBDI;

public class FrameMenu{

	private JFrame frmSimulaoConduo;
	private JFrame frmSimul;
	private JPanel typePanel = new JPanel();
	private JPanel buttonPanel = new JPanel();
	private JPanel choosePanel = new JPanel();
	JRadioButton work = new JRadioButton("Trabalho");
	JRadioButton leis = new JRadioButton("Passeio");
	JRadioButton rdbtnManual = new JRadioButton("Manual");
	JRadioButton rdbtnAutomtico = new JRadioButton("Autom\u00E1tico");
	JLabel type  = new JLabel("Tipo de Viagem");
	JLabel locals  = new JLabel("Locais");
	JTextField start = new JTextField(20);
	JTextField end = new JTextField(20);
	private final JLabel lblLocalDeDestion = new JLabel("Local de Destion");
	MapAgent city = new MapAgent();
	private final JPanel localsPanel = new JPanel();

	IComponentManagementService cms;
	ThreadSuspendable sus;

	public FrameMenu(IComponentManagementService cms, ThreadSuspendable sus) {
		this.cms=cms;
		this.sus=sus;

		initialize();
	}

	private void initialize() {


		try {
			city.setWorld(city.readGraph("City1"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			System.out.println("aqui");
		}

		for(int i=0; i<city.getWorld().getNodes().size();i++)
		{
			if(city.getWorld().getNodes().get(i).getValue() != 0)
			{
				JLabel l  = new JLabel(city.getWorld().getNodes().get(i).getName());
				l.setText(city.getWorld().getNodes().get(i).getName() + " - " + city.getWorld().getNodes().get(i).getValue()+"\n");
				localsPanel.add(l);
			}
		}

		frmSimulaoConduo = new JFrame();
		frmSimulaoConduo.setVisible(true);
		frmSimulaoConduo.setTitle("Simula\u00E7\u00E3o Condu\u00E7\u00E3o");
		frmSimulaoConduo.setBounds(100, 100, 600, 300);
		frmSimulaoConduo.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);



		ButtonGroup group = new ButtonGroup();
		group.add(work);
		group.add(leis);

		ButtonGroup AccGroup = new ButtonGroup();
		AccGroup.add(rdbtnManual);
		AccGroup.add(rdbtnAutomtico);

		JButton btnNewButton = new JButton("Iniciar Simulação");
		btnNewButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				String s = start.getText();
				String e = end.getText();
				int opt;
				int acc;

				if(work.isSelected())
					opt=1;
				else
					opt=2;

				if(rdbtnManual.isSelected())
					acc=1;
				else
					acc=2;

				Map<String, Object> agentArgs = new HashMap<String, Object>();
				agentArgs.put("start", s);
				agentArgs.put("end", e);
				agentArgs.put("opt", opt);

				CreationInfo OptionInfo = new CreationInfo(agentArgs);

				Map<String, Object> radioArgs = new HashMap<String, Object>();
				radioArgs.put("acc",acc);
				CreationInfo RadioInfo = new CreationInfo(radioArgs);

				frmSimulaoConduo.setVisible(false);

				IComponentIdentifier cid = cms.createComponent("DriverAgent", "agents/DriverAgentBDI.class", OptionInfo).getFirstResult(sus);
				System.out.println("Started simulation component: "+cid);

				IComponentIdentifier rid = cms.createComponent("RadioAgent", "agents/RadioAgentBDI.class", RadioInfo).getFirstResult(sus);
				System.out.println("Started simulation component: "+rid);

				IComponentIdentifier wid = cms.createComponent("WorldAgent", "agents/WorldAgentBDI.class", null).getFirstResult(sus);
				System.out.println("Started simulation component: "+wid);
				
				IComponentIdentifier oid = cms.createComponent("OtherAgent", "agents/OtherAgentBDI.class", null).getFirstResult(sus);
				System.out.println("Started simulation component: "+oid);


			}
		});

		type.setText("Tipo de viagem");work.setSelected(true);

		buttonPanel.add(btnNewButton);

		frmSimulaoConduo.getContentPane().add(typePanel, BorderLayout.WEST);

		locals.setText("Locais Disponíveis");

		JLabel lblAcidentes = new JLabel("Acidentes");

		GroupLayout gl_typePanel = new GroupLayout(typePanel);
		gl_typePanel.setHorizontalGroup(
				gl_typePanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_typePanel.createSequentialGroup()
						.addGap(108)
						.addGroup(gl_typePanel.createParallelGroup(Alignment.LEADING)
								.addComponent(locals)
								.addComponent(type))
								.addContainerGap(107, Short.MAX_VALUE))
								.addGroup(gl_typePanel.createSequentialGroup()
										.addGap(120)
										.addComponent(lblAcidentes)
										.addContainerGap(134, Short.MAX_VALUE))
										.addGroup(gl_typePanel.createSequentialGroup()
												.addGap(32)
												.addGroup(gl_typePanel.createParallelGroup(Alignment.LEADING)
														.addGroup(gl_typePanel.createSequentialGroup()
																.addComponent(rdbtnManual)
																.addGap(95)
																.addComponent(rdbtnAutomtico)
																.addContainerGap())
																.addGroup(gl_typePanel.createParallelGroup(Alignment.LEADING)
																		.addGroup(gl_typePanel.createSequentialGroup()
																				.addComponent(localsPanel, GroupLayout.PREFERRED_SIZE, 236, GroupLayout.PREFERRED_SIZE)
																				.addContainerGap())
																				.addGroup(gl_typePanel.createSequentialGroup()
																						.addComponent(work)
																						.addPreferredGap(ComponentPlacement.RELATED, 88, Short.MAX_VALUE)
																						.addComponent(leis)
																						.addGap(52)))))
				);
		rdbtnManual.setSelected(true);
		gl_typePanel.setVerticalGroup(
				gl_typePanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_typePanel.createSequentialGroup()
						.addContainerGap()
						.addComponent(type)
						.addGap(7)
						.addGroup(gl_typePanel.createParallelGroup(Alignment.BASELINE)
								.addComponent(work)
								.addComponent(leis))
								.addPreferredGap(ComponentPlacement.UNRELATED)
								.addComponent(lblAcidentes)
								.addPreferredGap(ComponentPlacement.RELATED)
								.addGroup(gl_typePanel.createParallelGroup(Alignment.BASELINE)
										.addComponent(rdbtnManual)
										.addComponent(rdbtnAutomtico))
										.addGap(30)
										.addComponent(locals)
										.addGap(18)
										.addComponent(localsPanel, GroupLayout.PREFERRED_SIZE, 45, GroupLayout.PREFERRED_SIZE)
										.addContainerGap(21, Short.MAX_VALUE))
				);
		typePanel.setLayout(gl_typePanel);
		frmSimulaoConduo.getContentPane().add(choosePanel, BorderLayout.CENTER);

		JLabel label = new JLabel("Local de Origem");
		GroupLayout gl_choosePanel = new GroupLayout(choosePanel);
		gl_choosePanel.setHorizontalGroup(
				gl_choosePanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_choosePanel.createSequentialGroup()
						.addContainerGap()
						.addGroup(gl_choosePanel.createParallelGroup(Alignment.LEADING)
								.addComponent(start, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(end, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(label)
								.addComponent(lblLocalDeDestion))
								.addContainerGap(166, Short.MAX_VALUE))
				);
		gl_choosePanel.setVerticalGroup(
				gl_choosePanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_choosePanel.createSequentialGroup()
						.addGap(29)
						.addComponent(label)
						.addPreferredGap(ComponentPlacement.UNRELATED)
						.addComponent(start, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addGap(16)
						.addComponent(lblLocalDeDestion)
						.addPreferredGap(ComponentPlacement.RELATED)
						.addComponent(end, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addContainerGap(99, Short.MAX_VALUE))
				);
		choosePanel.setLayout(gl_choosePanel);
		frmSimulaoConduo.getContentPane().add(buttonPanel, BorderLayout.SOUTH);
	}
}

