package gui;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

import agents.MapAgent;
import agents.RadioAgentBDI;
import agents.WorldAgentBDI;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;

import city.Graph;
import city.Node;

public class FrameInterface extends JPanel{

	Image node;
	Image carroD;
	Image carroL;
	Image carroR;
	Image carroU;
	Image accV;
	Image accH;
	Image nuvem;
	Image p1;
	Image p2;
	Image p3;
	Image p4;
	Image p5;
	Image p6;
	Image p7;
	Image p8;
	Image p9;
	public int CarX;
	public int CarY;
	public int CarDir;
	public int CarDist;
	public int currentCarX;
	public int currentCarY;
	public ArrayList<Accident> accs = new ArrayList<Accident>();
	public ArrayList<Weather> weas = new ArrayList<Weather>();
	public static MapAgent city = new MapAgent();
	MapAgent citytoClick = new MapAgent();

	public class Accident{
		private int x;
		private int y;
		private int ori;
		private int inter;
		private int dist;

		public Accident(int x, int y, int ori, int inter, double dist) {
			this.x=x;
			this.y=y;
			this.ori=ori;
			this.setInter(inter);
			Random r = new Random();
			int randomDist = r.nextInt((int) (dist-1)) + 1;
			this.dist = randomDist;
		}

		public int getX() {
			return x;
		}
		public void setX(int x) {
			this.x = x;
		}
		public int getY() {
			return y;
		}
		public void setY(int y) {
			this.y = y;
		}

		public int getOri() {
			return ori;
		}

		public void setOri(int ori) {
			this.ori = ori;
		}

		public int getInter() {
			return inter;
		}

		public void setInter(int inter) {
			this.inter = inter;
		}

		public int getDist() {
			return this.dist;
		}
	}

	public class Weather{
		private int x;
		private int y;
		private int ori;
		private int inter;
		private int dist;

		public Weather(int x, int y, int ori, int inter, double dist) {
			this.x=x;
			this.y=y;
			this.ori=ori;
			this.setInter(inter);
			Random r = new Random();
			int randomDist = r.nextInt((int) (dist-1)) + 1;
			this.dist = randomDist;
		}

		public int getX() {
			return x;
		}
		public void setX(int x) {
			this.x = x;
		}
		public int getY() {
			return y;
		}
		public void setY(int y) {
			this.y = y;
		}

		public int getOri() {
			return ori;
		}

		public void setOri(int ori) {
			this.ori = ori;
		}

		public int getInter() {
			return inter;
		}

		public void setInter(int inter) {
			this.inter = inter;
		}

		public int getDist() {
			return this.dist;
		}
	}


	JFrame cenario;

	public FrameInterface(Graph world) {

		try {
			citytoClick.setWorld(citytoClick.readGraph("City1"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
		}


		try {
			city.setWorld(city.readGraph("City1"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
		}

		cenario = new JFrame();
		cenario.setVisible(true);
		cenario.setTitle("Simula\u00E7\u00E3o Condu\u00E7\u00E3o");
		cenario.setExtendedState(JFrame.MAXIMIZED_BOTH); 
		cenario.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		setFocusable(true);
		setBackground(new Color(24,175,29));
		setDoubleBuffered(true);
		setVisible(true);

		cenario.add(this);

		cenario.addMouseListener(new MouseListener() {
			@Override
			public void mouseClicked(MouseEvent e) {


				for(int i = 0 ; i < citytoClick.getWorld().getEdges().size() ; i++) {
					Node n1 =  citytoClick.getWorld().getEdges().get(i).getNodes().get(0);
					Node n2 =  citytoClick.getWorld().getEdges().get(i).getNodes().get(1);

					int dir = seeDirection(n1,n2);

					int n1x = n1.getX()*60+15;
					int n1y = n1.getY()*60+15;
					int n2x = n2.getX()*60+15;
					int n2y = n2.getY()*60+15;

					int n1xaux;
					int n1yaux;


					if(dir <= 2) {
						n1x = n1x + 30;
						n1yaux = n1y + 30;
						if((e.getX() >= n1x && e.getX() <= n2x) &&(e.getY()-30 >= n1y && e.getY()-30 <= n1yaux)) {
							RadioAgentBDI.clicked=1;
							RadioAgentBDI.accidentEdge=i;

							Node nod1 = citytoClick.getWorld().getEdges().get(i).getNodes().get(0);
							Node nod2 = citytoClick.getWorld().getEdges().get(i).getNodes().get(1);

							for (int it = 0 ; it < citytoClick.getWorld().getEdges().size(); it++) {
								if (citytoClick.getWorld().getEdges().get(it).getNodes().get(0).equals(nod2) 
										&& citytoClick.getWorld().getEdges().get(it).getNodes().get(1).equals(nod1)){
									citytoClick.getWorld().getEdges().remove(i);
									citytoClick.getWorld().getEdges().remove(it-1);
									System.out.println("Removo - "+ i + " - " + (it-1));

								}
							}
						}
					}
					else{
						n1y = n1y + 30;
						n1xaux = n1x + 30; 

						if((e.getX() >= n1x && e.getX() <= n1xaux) &&(e.getY()-30 >= n1y && e.getY()-30 <= n2y)) {
							RadioAgentBDI.clicked=1;
							RadioAgentBDI.accidentEdge=i;
							Node nod1 = citytoClick.getWorld().getEdges().get(i).getNodes().get(0);
							Node nod2 = citytoClick.getWorld().getEdges().get(i).getNodes().get(1);

							for (int it = 0 ; it < citytoClick.getWorld().getEdges().size(); it++) {
								if (citytoClick.getWorld().getEdges().get(it).getNodes().get(0).equals(nod2) 
										&& citytoClick.getWorld().getEdges().get(it).getNodes().get(1).equals(nod1)){
									citytoClick.getWorld().getEdges().remove(i);
									citytoClick.getWorld().getEdges().remove(it-1);
									System.out.println("Removo - "+ i + " - " + (it-1));
								}
							}
						}
					}
				}

			}

			@Override
			public void mouseEntered(MouseEvent arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseExited(MouseEvent arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mousePressed(MouseEvent arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseReleased(MouseEvent arg0) {
				// TODO Auto-generated method stub

			}
		});



		ImageIcon ii = new ImageIcon("erva.png");
		node = ii.getImage();

		ImageIcon ii2 = new ImageIcon("cardown.png");
		carroD = ii2.getImage();
		ImageIcon ii3 = new ImageIcon("carleft.png");
		carroL = ii3.getImage();
		ImageIcon ii4 = new ImageIcon("carright.png");
		carroR = ii4.getImage();
		ImageIcon ii5 = new ImageIcon("carup.png");
		carroU = ii5.getImage();

		ImageIcon ii6 = new ImageIcon("accvert.png");
		accV = ii6.getImage();

		ImageIcon ii7 = new ImageIcon("acchor.png");
		accH = ii7.getImage();

		ImageIcon ii8 = new ImageIcon("weather.png");
		nuvem = ii8.getImage();
		
		ImageIcon ii9 = new ImageIcon("p0.png");
		p1 = ii9.getImage();
		ImageIcon ii10 = new ImageIcon("p2.png");
		p2 = ii10.getImage();
		ImageIcon ii11 = new ImageIcon("p3.png");
		p3 = ii11.getImage();
		ImageIcon ii12 = new ImageIcon("p4.png");
		p4 = ii12.getImage();
		ImageIcon ii13 = new ImageIcon("p5.png");
		p5 = ii13.getImage();
		ImageIcon ii14 = new ImageIcon("p6.png");
		p6 = ii14.getImage();
		ImageIcon ii15 = new ImageIcon("p7.png");
		p7 = ii15.getImage();
		ImageIcon ii16 = new ImageIcon("p8.png");
		p8 = ii16.getImage();
		ImageIcon ii17 = new ImageIcon("p9.png");
		p9 = ii17.getImage();
	}

	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D) g;

		for(int i=0; i<city.getWorld().getEdges().size(); i++)
		{
			Node n1 = city.getWorld().getEdges().get(i).getNodes().get(0);
			Node n2 = city.getWorld().getEdges().get(i).getNodes().get(1);

			int x1 = n1.getX()*60+30;
			int y1 = n1.getY()*60+30;

			int x2 = n2.getX()*60+30;
			int y2 = n2.getY()*60+30;

			g2d.setStroke(new BasicStroke(30));
			g2d.drawLine(x1, y1, x2, y2);

		}

		int image=0;
		for(int i=0; i<city.getWorld().getNodes().size(); i++)
		{
			if(city.getWorld().getNodes().get(i).getValue()!=0)
			{
				int x = city.getWorld().getNodes().get(i).getX()*60+15;
				int y = city.getWorld().getNodes().get(i).getY()*60+15;
				
				if(image==0)
				{
					g2d.drawImage(p1, x, y, null);
				}

				if(image==1)
				{
					g2d.drawImage(p2, x, y, null);
				}

				if(image==2)
				{
					g2d.drawImage(p3, x, y, null);
				}

				if(image==3)
				{
					g2d.drawImage(p4, x, y, null);
				}

				if(image==4)
				{
					g2d.drawImage(p5, x, y, null);
				}

				if(image==5)
				{
					g2d.drawImage(p6, x, y, null);
				}

				if(image==6)
				{
					g2d.drawImage(p7, x, y, null);
				}

				if(image==7)
				{
					g2d.drawImage(p8, x, y, null);
				}

				if(image==8)
				{
					g2d.drawImage(p9, x, y, null);
				}
				
				g2d.setFont(new Font("Franklin Gothic Medium", Font.PLAIN, 20));
				g2d.setColor(Color.YELLOW);
				g2d.drawString(city.getWorld().getNodes().get(i).getName() + " - " + city.getWorld().getNodes().get(i).getValue(), x, y);
				image++;
			}

		}
		//direita
		if(CarDir==1)
		{
			currentCarX=CarX*60+15+CarDist;
			currentCarY=CarY*60+15;
			g2d.drawImage(carroR, CarX*60+15+CarDist, CarY*60+15, null);

		}
		//esquerda
		if(CarDir==2)
		{
			currentCarX=CarX*60+15-CarDist;
			currentCarY=CarY*60+15;
			g2d.drawImage(carroL, CarX*60+15-CarDist, CarY*60+15, null);
		}
		//cima
		if(CarDir==3)
		{
			currentCarX=CarX*60+15;
			currentCarY=CarY*60+15-CarDist;
			g2d.drawImage(carroU, CarX*60+15, CarY*60+15-CarDist, null);
		}
		//baixo
		if(CarDir==4)
		{
			currentCarX=CarX*60+15;
			currentCarY=CarY*60+15+CarDist;
			g2d.drawImage(carroD, CarX*60+15, CarY*60+15+CarDist, null);
		}

		for(int i=0; i<accs.size(); i++)
		{
			if(accs.get(i).getOri()<=2)
				if(accs.get(i).getInter()==0)
				{
					if(accs.get(i).getOri() == 1)
						g2d.drawImage(accV,accs.get(i).getX()+accs.get(i).getDist()*60, accs.get(i).getY()-8, null);
					if(accs.get(i).getOri() == 2)
						g2d.drawImage(accV,accs.get(i).getX()-accs.get(i).getDist()*60, accs.get(i).getY()-8, null);
				}
				else{
					if(accs.get(i).getOri() == 1)
						g2d.drawImage(accV,accs.get(i).getX()+30, accs.get(i).getY()-8, null);
					if(accs.get(i).getOri() == 2)
						g2d.drawImage(accV,accs.get(i).getX()-30, accs.get(i).getY()-8, null);
				}

			if(accs.get(i).getOri()>2)
				if(accs.get(i).getInter()==0)
				{
					if(accs.get(i).getOri() == 3)
						g2d.drawImage(accH,accs.get(i).getX()-8, accs.get(i).getY()-accs.get(i).getDist()*60, null);
					if(accs.get(i).getOri() == 4)
						g2d.drawImage(accH,accs.get(i).getX()-8, accs.get(i).getY()+accs.get(i).getDist()*60, null);
				}
				else{
					if(accs.get(i).getOri() == 3)
						g2d.drawImage(accH,accs.get(i).getX()-8, accs.get(i).getY()-30, null);
					if(accs.get(i).getOri() == 4)
						g2d.drawImage(accH,accs.get(i).getX()-8, accs.get(i).getY()+30, null);
				}
		}

		for(int i=0; i<weas.size(); i++)
		{
			if(weas.get(i).getOri()<=2)
				if(weas.get(i).getInter()==0)
				{
					if(weas.get(i).getOri() == 1)
						g2d.drawImage(nuvem,weas.get(i).getX()+weas.get(i).getDist()*60, weas.get(i).getY()-8, null);
					if(weas.get(i).getOri() == 2)
						g2d.drawImage(nuvem,weas.get(i).getX()-weas.get(i).getDist()*60, weas.get(i).getY()-8, null);
				}
				else{
					if(weas.get(i).getOri() == 1)
						g2d.drawImage(nuvem,weas.get(i).getX()+30, weas.get(i).getY()-8, null);
					if(weas.get(i).getOri() == 2)
						g2d.drawImage(nuvem,weas.get(i).getX()-30, weas.get(i).getY()-8, null);
				}

			if(weas.get(i).getOri()>2)
				if(weas.get(i).getInter()==0)
				{
					if(weas.get(i).getOri() == 3)
						g2d.drawImage(nuvem,weas.get(i).getX()-8, weas.get(i).getY()-weas.get(i).getDist()*60, null);
					if(weas.get(i).getOri() == 4)
						g2d.drawImage(nuvem,weas.get(i).getX()-8, weas.get(i).getY()+weas.get(i).getDist()*60, null);
				}
				else{
					if(weas.get(i).getOri() == 3)
						g2d.drawImage(nuvem,weas.get(i).getX()-8, weas.get(i).getY()-30, null);
					if(weas.get(i).getOri() == 4)
						g2d.drawImage(nuvem,weas.get(i).getX()-8, weas.get(i).getY()+30, null);
				}
		}

	}

	public int seeDirection(Node n1, Node n2)
	{
		//direita = 1
		if(n1.getX()-n2.getX()<0)
			return 1;
		//esquerda = 2
		else if(n1.getX()-n2.getX()>0)
			return 2;
		//baixo = 3
		else if(n1.getY()-n2.getY()>0)
			return 3;
		//cima = 4
		else if(n1.getY()-n2.getY()<0)
			return 4;
		else
			return 5;
	}

}
