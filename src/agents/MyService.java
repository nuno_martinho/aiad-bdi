package agents;

import city.Edge;
import jadex.commons.future.IFuture;

public interface MyService {
  public IFuture<Void> serviceAcc(int value, int e);
  public IFuture<Void> serviceWea(int value, int e);
  public IFuture<Void> serviceLei(int lei, String leiNodeName);
}