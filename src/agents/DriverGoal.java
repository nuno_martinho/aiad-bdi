package agents;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.LinkedList;

import city.Edge;
import city.Graph;
import city.Leisure;
import city.Node;
import city.Shortest;
import jadex.bdiv3.annotation.Goal;

@Goal
public class DriverGoal {

	private Graph World;
	private String origem;
	private String destino;
	private String destino_final;
	private int opt;
	private Shortest sol1;
	private Leisure sol2;
	private LinkedList<Node> path;
	private ArrayList<Edge> edges;

	public DriverGoal(Graph World, String origem, String destino,int opt) {
		this.World = World;
		this.origem = origem;
		this.destino = destino;
		this.opt = opt;
	}


	//L� o grafo e guarda numa estrutur
	public Graph getWorld() {
		return World;
	}

	public void setWorld(Graph world) {
		World = world;
	}

	public String getOrigem(){
		return origem;
	}
	public void setOrigem(String origem){
		
		this.origem = origem;
	}

	public String getDestino(){
		return destino;
	}
	public void setDestino(String destino){
		
		this.destino = destino;
	}
	
	public String getDestinoFinal(){
		return destino_final;
	}
	
	public void setDestinoFinal(String destino_final){
		this.destino_final = destino_final;
	}
	
	public int getOpt(){
		return opt;
	}
	public void setOpt(int option){
		this.opt = opt;
		
	}
	
	public LinkedList<Node> getPath(){
		return path;
	}
	public void setPath(LinkedList<Node> path){
		this.path = path;
	}
	
	public ArrayList<Edge> getEdges(){
		return edges;
	}
	
}
