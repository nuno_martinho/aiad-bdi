package agents;
import jadex.bdiv3.BDIAgent;
import jadex.bridge.service.RequiredServiceInfo;
import jadex.bridge.service.annotation.Service;
import jadex.bridge.service.search.SServiceProvider;
import jadex.commons.future.DefaultResultListener;
import jadex.micro.annotation.Agent;
import jadex.micro.annotation.AgentBody;

import java.io.IOException;
import java.util.Random;

@Agent
@Service
public class WorldAgentBDI {

	@Agent
	protected BDIAgent WorldAgent;
	public static MapAgent city = new MapAgent();

	public int i=0;

	@AgentBody
	public void body()
	{
		try {
			city.setWorld(city.readGraph("City1"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		SServiceProvider.getService(WorldAgent.getServiceProvider(), WorldService.class, RequiredServiceInfo.SCOPE_PLATFORM)
		.addResultListener(new DefaultResultListener<WorldService>() {
			public void resultAvailable(WorldService cs) {
				tryAtmosferic(cs);
			}
		});
	}

	public void tryAtmosferic(WorldService cs){
		for(int j=0; j<100;j++)
		{
			Random r = new Random();
			WorldAgent.waitForDelay(1000).get();
			int randomNum = r.nextInt(10) + 1;
			
			if(randomNum<=2)
			{
				this.i++;

				int randomEdge = r.nextInt(city.getWorld().getEdges().size()-1) + 1;
				
				

				cs.serviceWeather(i,randomEdge);
			}
		}
	}

}

