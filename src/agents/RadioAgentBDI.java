package agents;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import city.Edge;
import city.Node;
import jadex.bdiv3.BDIAgent;
import jadex.bdiv3.annotation.Belief;
import jadex.bdiv3.annotation.Plan;
import jadex.bdiv3.annotation.Trigger;
import jadex.bridge.service.RequiredServiceInfo;
import jadex.bridge.service.annotation.Service;
import jadex.bridge.service.search.SServiceProvider;
import jadex.commons.future.DefaultResultListener;
import jadex.commons.future.IFuture;
import jadex.micro.annotation.Agent;
import jadex.micro.annotation.AgentBody;
import jadex.micro.annotation.Arguments;
import jadex.micro.annotation.ProvidedService;
import jadex.micro.annotation.ProvidedServices;

@Arguments
@Agent
@Service
@ProvidedServices(@ProvidedService(type=WorldService.class))
public class RadioAgentBDI implements WorldService{

	@Agent
	protected BDIAgent RadioAgent;

	public int i=0;
	public static int clicked;
	public static int accidentEdge;
	MapAgent city = new MapAgent();
	private int weather;
	private int weatEdgeID;
	public static int lei;
	public static String leiNodeName;
	public int acc;

	@AgentBody
	public void body()
	{
		try {
			city.setWorld(city.readGraph("City1"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Map<String, Object> radioArgs = RadioAgent.getArguments();
		acc = (Integer) radioArgs.get("acc");
		
		SServiceProvider.getService(RadioAgent.getServiceProvider(), MyService.class, RequiredServiceInfo.SCOPE_PLATFORM)
		.addResultListener(new DefaultResultListener<MyService>() {
			public void resultAvailable(MyService cs) {

				for(int i=0; i<city.getWorld().getEdges().size();i++ ){
					System.out.print(i + " - " + city.getWorld().getEdges().get(i).getNodes().get(0).getName());
					System.out.println( " -> "+ city.getWorld().getEdges().get(i).getNodes().get(1).getName());
				}

				while(true)
				{
					RadioAgent.waitForDelay(1).get();
					if(weather!=0)
					{
						System.out.println("Mudou o tempo em "+ weatEdgeID);
						cs.serviceWea(weather, weatEdgeID);
						weather=0;
					}
					
					if(lei!=0)
					{
						System.out.println("Mudou o valor "+ leiNodeName);
						cs.serviceLei(lei, leiNodeName);
						lei=0;
					}
					
					if(acc==1)
					{
						System.out.print("");
						if (clicked == 1) {
							System.out.println("Acidente em " + accidentEdge);
							cs.serviceAcc(11,accidentEdge);
							clicked=0;
						}
						
					}
					else
						tryAccident(cs);
				}

			}
		});
	}

	public void tryAccident(MyService cs){
		RadioAgent.waitForDelay(1000).get();

		Random r = new Random();

		int randomNum = r.nextInt(10) + 1;
		if(randomNum<=3)
		{
			this.i++;

			int randomEdge = r.nextInt(city.getWorld().getEdges().size()-1) + 1;
			
			Node nod1 = city.getWorld().getEdges().get(randomEdge).getNodes().get(0);
			Node nod2 = city.getWorld().getEdges().get(randomEdge).getNodes().get(1);

			for (int it = 0 ; it < city.getWorld().getEdges().size(); it++) {
				if (city.getWorld().getEdges().get(it).getNodes().get(0).equals(nod2) 
						&& city.getWorld().getEdges().get(it).getNodes().get(1).equals(nod1)){
				if(randomEdge>(it))
				{
					city.getWorld().getEdges().remove(randomEdge);
					city.getWorld().getEdges().remove(it);

					System.out.println("Removo - "+ it + " - " + randomEdge);
					System.out.println("Nos - "+ nod1.getName() + " - " + nod2.getName());

					cs.serviceAcc(i,it);
				}
				else{

					city.getWorld().getEdges().remove(randomEdge);
					city.getWorld().getEdges().remove(it-1);

					System.out.println("Removo - "+ randomEdge + " - " + (it-1));
					System.out.println("Nos - "+ nod1.getName() + " - " + nod2.getName());

					cs.serviceAcc(i,randomEdge);
				}


				}
			}
			
		}
	}

	public static int getClicked() {
		return clicked;
	}
	
	@Plan(trigger=@Trigger(factchangeds="weather"))
	public void checkNewValuePlan(int v) {
	}
	
	@Belief
	public int getWeather() {
		return weather;
	}

	@Belief
	public void setWeather(int n, int e) {
		this.weather=n;
		this.weatEdgeID=e;
	}
	
	@Plan(trigger=@Trigger(factchangeds="lei"))
	public void checkNewValue(int v) {
	}
	
	@Belief
	public int getLei() {
		return lei;
	}

	@Belief
	public void setLei(int n, String e) {
		this.lei=n;
		this.leiNodeName=e;
	}

	@Override
	public IFuture<Void> serviceWeather(int value, int e) {
		this.setWeather(value, e);
		return null;
	}

}