
package agents;

import gui.FrameInterface;
import gui.FrameInterface.Accident;
import gui.FrameInterface.Weather;
import jadex.bdi.runtime.PlanFailureException;
import jadex.bdiv3.BDIAgent;
import jadex.bdiv3.annotation.Belief;
import jadex.bdiv3.annotation.Plan;
import jadex.bdiv3.annotation.Trigger;
import jadex.bridge.IComponentIdentifier;
import jadex.bridge.service.annotation.Service;
import jadex.commons.future.IFuture;
import jadex.micro.annotation.Agent;
import jadex.micro.annotation.AgentBody;
import jadex.micro.annotation.Arguments;
import jadex.micro.annotation.ProvidedService;
import jadex.micro.annotation.ProvidedServices;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import city.Edge;
import city.Graph;
import city.Leisure;
import city.Node;
import city.Shortest;

@Agent
@Arguments
@Service
@ProvidedServices(@ProvidedService(type=MyService.class))
public class DriverAgentBDI implements MyService  {
	@Agent
	protected BDIAgent agent;
	private int previousNode;
	private int accident;
	private int accEdgeID;
	private int wea;
	private int weaEdgeID;
	private int lei;
	private String leiNodeName;
	private double edgeDist;
	boolean moving = false;
	Timer timer;
	FrameInterface f;
	Graph World;

	@AgentBody
	public void body() {			
		try {
			int interfere = 0;
			int intRC = 0;
			Map<String, Object> agentArgs = agent.getArguments();

			String start = (String) agentArgs.get("start");
			String end = (String) agentArgs.get("end");
			int opt = (Integer) agentArgs.get("opt");

			DriverGoal goal = (DriverGoal) agent.dispatchTopLevelGoal(new DriverGoal(
					World = readGraph("City1"),start,end,opt)).get();

			
			for(int i=0; i<World.getEdges().size();i++ ){
				System.out.print(i + " - " + World.getEdges().get(i).getNodes().get(0).getName());
				System.out.println( " -> "+ World.getEdges().get(i).getNodes().get(1).getName());
			}

			
			
			for(int f = 0 ; f < goal.getWorld().getEdges().size(); f++) {
				goal.getWorld().getEdges().get(f).getNodes().get(0).getEdges().add(goal.getWorld().getEdges().get(f));
				goal.getWorld().getEdges().get(f).getNodes().get(1).getEdges().add(goal.getWorld().getEdges().get(f));
			}

			f = new FrameInterface(World);

			for(int i=0; i<(goal.getPath()).size(); i++)
			{
				
				System.out.println(goal.getPath().get(i).getName());
				if(!goal.getPath().get(i).getName().equals(end)){

					double dist = goal.getWorld().getDistanceByNodes(goal.getPath().get(i).getName(), goal.getPath().get(i+1).getName());
					int x = goal.getPath().get(i).getX();
					int y = goal.getPath().get(i).getY();

					f.CarX = x;
					f.CarY = y;

					dist=60*dist;
					int temp = 0;

					while(temp<dist)
					{
						agent.waitForDelay(1).get();
						if(accident != 0 || wea!=0){
							//interfere
							int criou=1;
							Node n1 = null;
							Node n2 = null;
							
							if(accident!=0)
							{
								n1 = World.getEdges().get(accEdgeID).getNodes().get(0);
								n2 = World.getEdges().get(accEdgeID).getNodes().get(1);
								this.edgeDist = World.getEdges().get(accEdgeID).getDistance();
							}
							if(wea!=0)
							{
								n1 = World.getEdges().get(weaEdgeID).getNodes().get(0);
								n2 = World.getEdges().get(weaEdgeID).getNodes().get(1);
								this.edgeDist = World.getEdges().get(weaEdgeID).getDistance();
							}
							int roadDir = seeDirection(n1, n2);

							for (int k = i ; k < goal.getPath().size(); k++){
								if(goal.getPath().get(k).equals(n1) && goal.getPath().get(k+1).equals(n2)){
									interfere = 1;
									System.out.println("intefere");
								}
							}

							//volta atras
							if (interfere == 1){
								if(goal.getPath().get(i).equals(n1) && goal.getPath().get(i+1).equals(n2))
									intRC=1;

								int dir = seeDirection(goal.getPath().get(i), goal.getPath().get(i+1));
								f.CarDir=dir;

								if(intRC==1)
								{	
									//adicionar weather
									if(wea!=0)
									{
										Weather nuv = f.new Weather(f.currentCarX,f.currentCarY,seeDirection(goal.getPath().get(i), goal.getPath().get(i+1)), 1, edgeDist);
										f.weas.add(nuv);
									}
									if(accident!=0)
									{
										Accident estouro = f.new Accident(f.currentCarX,f.currentCarY,seeDirection(goal.getPath().get(i), goal.getPath().get(i+1)), 1, edgeDist);
										f.accs.add(estouro);
									}

								}
								else{
									if(wea!=0)
									{
										Weather nuv = f.new Weather(n1.getX()*60+15,n1.getY()*60+15,roadDir,0,edgeDist);
										f.weas.add(nuv);
									}
									if(accident!=0)
									{
										Accident estouro = f.new Accident(n1.getX()*60+15,n1.getY()*60+15,roadDir,0,edgeDist);
										f.accs.add(estouro);
									}
								}
								int thisedge = 0;

								if(intRC==0)
								{
									DriverGoal auxgoal = (DriverGoal) agent.dispatchTopLevelGoal(new DriverGoal(World,goal.getPath().get(i).getName(),end,opt)).get();

									for(int f = 0 ; f < auxgoal.getWorld().getEdges().size() ; f++) {
										if(auxgoal.getWorld().getEdges().get(f).getID() == accEdgeID) {
											System.out.println("node 1 - " + auxgoal.getPath().get(0).getName() + " " + auxgoal.getPath().get(0).getEdges().size()/2);
											System.out.println("node 2 - " + auxgoal.getPath().get(1).getName() + " " + auxgoal.getPath().get(1).getEdges().size()/2);
											if(auxgoal.getWorld().getEdges().get(f).getNodes().contains(auxgoal.getPath().get(1)) && auxgoal.getPath().get(1).getEdges().size()/2 <= 2 ||
													auxgoal.getWorld().getEdges().get(f).getNodes().contains(auxgoal.getPath().get(0)) && auxgoal.getPath().get(0).getEdges().size()/2 <= 2){
												thisedge = 1;
											}
										}
									}

								}
								else
								{
									thisedge = 1;
								}

								intRC=0;
								if(thisedge == 1) {
									while(temp>0)
									{
										if(moving==false){ 
											new Mover(10);
											--temp;
										}

										f.CarDist= (int) temp;

										f.repaint();

									}
									thisedge = 0;
									if(wea!=0)
									{
										World.getEdges().get(weaEdgeID).setDistance(World.getEdges().get(weaEdgeID).getDistance()*2);
									}
									if(accident!=0)
									{
										Node nod1 = World.getEdges().get(accEdgeID).getNodes().get(0);
										Node nod2 = World.getEdges().get(accEdgeID).getNodes().get(1);

										for (int it = 0 ; it < World.getEdges().size(); it++) {
											if (World.getEdges().get(it).getNodes().get(0).equals(nod2) 
													&& World.getEdges().get(it).getNodes().get(1).equals(nod1)){
												World.getEdges().remove(accEdgeID);
												World.getEdges().remove(it-1);

												System.out.println("Removo - "+ accEdgeID + " - " + (it-1));
												System.out.println("Nos - "+ nod1.getName() + " - " + nod2.getName());

											}
										}

										Node nodz1 = WorldAgentBDI.city.getWorld().getEdges().get(accEdgeID).getNodes().get(0);
										Node nodz2 = WorldAgentBDI.city.getWorld().getEdges().get(accEdgeID).getNodes().get(1);

										for (int it = 0 ; it < WorldAgentBDI.city.getWorld().getEdges().size(); it++) {
											if (WorldAgentBDI.city.getWorld().getEdges().get(it).getNodes().get(0).equals(nodz2) 
													&& WorldAgentBDI.city.getWorld().getEdges().get(it).getNodes().get(1).equals(nodz1)){
												WorldAgentBDI.city.getWorld().getEdges().remove(accEdgeID);
												WorldAgentBDI.city.getWorld().getEdges().remove(it-1);

												System.out.println("Removo - "+ accEdgeID + " - " + (it-1));
												System.out.println("Nos - "+ nodz1.getName() + " - " + nodz2.getName());

											}
										}


									}
									goal = (DriverGoal) agent.dispatchTopLevelGoal(new DriverGoal(World,goal.getPath().get(i).getName(),end,opt)).get();
									i = -1;

									accident=0;
									wea=0;
									interfere=0;
									break;

								}
								else{
									while(temp<dist)
									{
										if(moving==false){ 
											new Mover(10);
											++temp;
										}

										f.CarDist= (int) temp;

										f.repaint();

									}
									if(wea!=0)
									{
										World.getEdges().get(weaEdgeID).setDistance(World.getEdges().get(weaEdgeID).getDistance()*2);
									}
									if(accident!=0)
									{

										Node nod1 = World.getEdges().get(accEdgeID).getNodes().get(0);
										Node nod2 = World.getEdges().get(accEdgeID).getNodes().get(1);

										for (int it = 0 ; it < World.getEdges().size(); it++) {
											if (World.getEdges().get(it).getNodes().get(0).equals(nod2) 
													&& World.getEdges().get(it).getNodes().get(1).equals(nod1)){
												World.getEdges().remove(accEdgeID);
												World.getEdges().remove(it-1);

												System.out.println("Removo - "+ accEdgeID + " - " + (it-1));
												System.out.println("Nos - "+ nod1.getName() + " - " + nod2.getName());
											}
										}

										Node nodz1 = WorldAgentBDI.city.getWorld().getEdges().get(accEdgeID).getNodes().get(0);
										Node nodz2 = WorldAgentBDI.city.getWorld().getEdges().get(accEdgeID).getNodes().get(1);

										for (int it = 0 ; it < WorldAgentBDI.city.getWorld().getEdges().size(); it++) {
											if (WorldAgentBDI.city.getWorld().getEdges().get(it).getNodes().get(0).equals(nodz2) 
													&& WorldAgentBDI.city.getWorld().getEdges().get(it).getNodes().get(1).equals(nodz1)){
												WorldAgentBDI.city.getWorld().getEdges().remove(accEdgeID);
												WorldAgentBDI.city.getWorld().getEdges().remove(it-1);

												System.out.println("Removo - "+ accEdgeID + " - " + (it-1));
												System.out.println("Nos - "+ nodz1.getName() + " - " + nodz2.getName());

											}
										}

									}
									goal = (DriverGoal) agent.dispatchTopLevelGoal(new DriverGoal(World,goal.getPath().get(i+1).getName(),end,opt)).get();
									i=-1;

									accident=0;
									wea=0;
									interfere=0;
									break;

								}
							}
							else{
								if(criou==1)
								{
									//muda o peso e adiciona o weather
									if(wea!=0)
									{
										World.getEdges().get(weaEdgeID).setDistance(World.getEdges().get(weaEdgeID).getDistance()*2);
										Weather nuv = f.new Weather(n1.getX()*60+15,n1.getY()*60+15,roadDir,0,edgeDist);
										f.weas.add(nuv);
									}
									if(accident!=0)
									{
										Node nod1 = World.getEdges().get(accEdgeID).getNodes().get(0);
										Node nod2 = World.getEdges().get(accEdgeID).getNodes().get(1);

										for (int it = 0 ; it < World.getEdges().size(); it++) {
											if (World.getEdges().get(it).getNodes().get(0).equals(nod2) 
													&& World.getEdges().get(it).getNodes().get(1).equals(nod1)){
												World.getEdges().remove(accEdgeID);
												World.getEdges().remove(it-1);

												System.out.println("Removo - "+ accEdgeID + " - " + (it-1));
												System.out.println("Nos - "+ nod1.getName() + " - " + nod2.getName());

											}
										}

										Node nodz1 = WorldAgentBDI.city.getWorld().getEdges().get(accEdgeID).getNodes().get(0);
										Node nodz2 = WorldAgentBDI.city.getWorld().getEdges().get(accEdgeID).getNodes().get(1);

										for (int it = 0 ; it < WorldAgentBDI.city.getWorld().getEdges().size(); it++) {
											if (WorldAgentBDI.city.getWorld().getEdges().get(it).getNodes().get(0).equals(nodz2) 
													&& WorldAgentBDI.city.getWorld().getEdges().get(it).getNodes().get(1).equals(nodz1)){
												WorldAgentBDI.city.getWorld().getEdges().remove(accEdgeID);
												WorldAgentBDI.city.getWorld().getEdges().remove(it-1);

												System.out.println("Removo - "+ accEdgeID + " - " + (it-1));
												System.out.println("Nos - "+ nodz1.getName() + " - " + nodz2.getName());

											}
										}

										Accident estouro = f.new Accident(n1.getX()*60+15,n1.getY()*60+15,roadDir,0,edgeDist);
										f.accs.add(estouro);
									}
									accident=0;
									wea=0;
									interfere=0;
									criou=0;
								}
							}
						}

						//desenho normal
						int dir = seeDirection(goal.getPath().get(i), goal.getPath().get(i+1));
						f.CarDir=dir;
						if(moving==false){ 
							new Mover(10);
							++temp;
						}

						f.CarDist= (int) temp;

						f.repaint();
					}

				}

			}

			System.out.println(goal.getPath().getLast().getName());
			System.out.println("Chegou ao destino");


		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Plan(trigger=@Trigger(goals=DriverGoal.class))
	protected void ShortestPath(DriverGoal goal){
		if (goal.getOpt() == 2)
			throw new PlanFailureException();
		LinkedList<Node> path = null;
		Shortest dijkstra = new Shortest(goal.getWorld());
		for(int i=0; i<goal.getWorld().getNodes().size(); i++)
		{
			if(goal.getWorld().getNodes().get(i).getName().equals(goal.getOrigem()))
				dijkstra.execute(goal.getWorld().getNodes().get(i));
		}

		for(int i=0; i<goal.getWorld().getNodes().size(); i++)
		{
			if(goal.getWorld().getNodes().get(i).getName().equals(goal.getDestino()))
			{
				path = dijkstra.getPath(goal.getWorld().getNodes().get(i));
			}
		}
		goal.setPath(path);
		//getEdgesPath(goal);

	}

	@Plan(trigger=@Trigger(goals=DriverGoal.class))
	protected void LeisurePath(DriverGoal goal){
		if (goal.getOpt() == 1)
			throw new PlanFailureException();
		LinkedList<Node> path = null;
		Leisure leis = new Leisure(goal.getWorld());
		for(int i=0; i<goal.getWorld().getNodes().size(); i++)
		{
			if(goal.getWorld().getNodes().get(i).getName().equals(goal.getOrigem()))
				leis.execute(goal.getWorld().getNodes().get(i));
		}

		for(int i=0; i<goal.getWorld().getNodes().size(); i++)
		{
			if(goal.getWorld().getNodes().get(i).getName().equals(goal.getDestino()))
			{
				path = leis.getPath(goal.getWorld().getNodes().get(i));
			}
		}
		goal.setPath(path);
	}

	@Plan(trigger=@Trigger(factchangeds="accident"))
	public void checkAccident(int v) {
	}


	@Plan(trigger=@Trigger(factchangeds="wea"))
	public void checkWeather(int a) {
	}
	
	@Plan(trigger=@Trigger(factchangeds="lei"))
	public void checkLeisure(int a) {
		for(int u=0; u<World.getNodes().size(); u++)
		{
			if(World.getNodes().get(u).getName().equals(leiNodeName))
			{
				World.getNodes().get(u).setValue(World.getNodes().get(u).getValue()+lei);
				FrameInterface.city.getWorld().getNodes().get(u).setValue(World.getNodes().get(u).getValue()+lei);
			}
		}
	}


	public Graph readGraph(String filename) throws IOException
	{
		Graph temp = new Graph(filename);

		BufferedReader reader;
		try {
			reader = new BufferedReader(new FileReader(filename));
			String line;
			while ((line = reader.readLine()) != null)
			{
				String parts[] = line.split(" ");
				if(parts[1].equals("--")||parts[1].equals("->"))
				{
					String node1 = parts[0];
					String node2 = parts[2];
					String direction = parts[1];
					String distance = parts [3];

					//Para o 1� node
					boolean exists1=false;
					Node n = new Node(0,node1,0,0);
					for(int i=0; i<temp.getNodes().size();i++)
					{
						if(temp.getNodes().get(i).getName().equals(node1))
						{
							exists1=true;
						}
					}

					if(exists1==false)
					{
						temp.getNodes().add(n);
					}

					//Para o 2� node
					boolean exists2=false;
					Node n2 = new Node(0,node2,0,0);
					for(int i=0; i<temp.getNodes().size();i++)
					{
						if(temp.getNodes().get(i).getName().equals(node2))
						{
							exists2=true;
						}
					}

					if(exists2==false)
					{
						temp.getNodes().add(n2);
					}

					//Guardar arestas
					Edge e = new Edge(Double.parseDouble(distance), temp.getEdges().size());
					for(int i=0; i<temp.getNodes().size();i++)
					{
						if(temp.getNodes().get(i).getName().equals(n.getName()))
						{
							e.getNodes().add(temp.getNodes().get(i));
							temp.getNodes().get(i).incNmrEdges();
						}
					}
					for(int i=0; i<temp.getNodes().size();i++)
					{
						if(temp.getNodes().get(i).getName().equals(n2.getName()))
						{
							e.getNodes().add(temp.getNodes().get(i));
							temp.getNodes().get(i).incNmrEdges();
						}
					}
					temp.getEdges().add(e);

				}
				else{

					String node = parts[0];
					String coordinates[] = parts[1].split(",");
					String x = coordinates[0];
					String y = coordinates[1];
					String value = parts[2];

					for(int i=0; i<temp.getNodes().size();i++)
					{
						if(temp.getNodes().get(i).getName().equals(node))
						{
							temp.getNodes().get(i).setValue(Double.parseDouble(value));
							temp.getNodes().get(i).setX(Integer.parseInt(x));
							temp.getNodes().get(i).setY(Integer.parseInt(y));
						}
					}
				}
			}
			reader.close();
			return temp;
		} 
		catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return temp;
		}

	}

	public int seeDirection(Node n1, Node n2)
	{
		//direita = 1
		if(n1.getX()-n2.getX()<0)
			return 1;
		//esquerda = 2
		else if(n1.getX()-n2.getX()>0)
			return 2;
		//baixo = 3
		else if(n1.getY()-n2.getY()>0)
			return 3;
		//cima = 4
		else if(n1.getY()-n2.getY()<0)
			return 4;
		else
			return 5;
	}

	@Belief
	public int getAccident() {
		return accident;
	}

	@Belief
	public void setAccident(int n, int e) {
		this.accident=n;
		this.setAccEdge(e);
	}

	@Belief
	public int getWea() {
		return wea;
	}

	@Belief
	public void setWea(int n, int e) {
		this.wea=n;
		this.setWeaEdge(e);
	}
	
	@Belief
	public int getLei() {
		return lei;
	}

	@Belief
	public void setLei(int n, String e) {
		this.lei=n;
		this.setLeiNode(e);
	}
	
	public void setLeiNode(String e) {
		this.leiNodeName=e;
	}

	public class Mover {
		Timer timer;

		public Mover(int seconds) {
			timer = new Timer();
			moving = true;
			timer.schedule(new MoveTime(), seconds);
		}

		class MoveTime extends TimerTask {
			public void run() {
				moving = false;
				timer.cancel(); //Terminate the timer thread
			}
		}

	}

	@Override
	public IFuture<Void> serviceAcc(int value,int e){
		this.setAccident(value, e);
		return null;
	}

	public int getAccEdge() {
		return accEdgeID;
	}

	public void setAccEdge(int accEdge) {
		this.accEdgeID = accEdge;
	}

	public int getWeaEdge() {
		return accEdgeID;
	}

	private void setWeaEdge(int e) {
		this.weaEdgeID = e;
	}

	@Override
	public IFuture<Void> serviceWea(int value, int e) {
		this.setWea(value, e);
		return null;
	}

	@Override
	public IFuture<Void> serviceLei(int lei, String leiNodeName) {
		this.setLei(lei, leiNodeName);
		return null;
	}
}