package agents;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import city.Edge;
import city.Graph;
import city.Node;

public class MapAgent {


	private Graph World;


	//L� o grafo e guarda numa estrutura
	public Graph readGraph(String filename) throws IOException
	{
		Graph temp = new Graph(filename);

		BufferedReader reader;
		try {
			reader = new BufferedReader(new FileReader(filename));
			String line;
			while ((line = reader.readLine()) != null)
			{
				String parts[] = line.split(" ");
				if(parts[1].equals("--")||parts[1].equals("->"))
				{
					String node1 = parts[0];
					String node2 = parts[2];
					String direction = parts[1];
					String distance = parts [3];

					//Para o 1� node
					boolean exists1=false;
					Node n = new Node(0,node1,0,0);
					for(int i=0; i<temp.getNodes().size();i++)
					{
						if(temp.getNodes().get(i).getName().equals(node1))
						{
							exists1=true;
						}
					}

					if(exists1==false)
					{
						temp.getNodes().add(n);
					}

					//Para o 2� node
					boolean exists2=false;
					Node n2 = new Node(0,node2,0,0);
					for(int i=0; i<temp.getNodes().size();i++)
					{
						if(temp.getNodes().get(i).getName().equals(node2))
						{
							exists2=true;
						}
					}

					if(exists2==false)
					{
						temp.getNodes().add(n2);
					}

					//Guardar arestas
					Edge e = new Edge(Double.parseDouble(distance), temp.getEdges().size());
					
					for(int i=0; i<temp.getNodes().size();i++)
					{
						if(temp.getNodes().get(i).getName().equals(n.getName()))
						{
							e.getNodes().add(temp.getNodes().get(i));
							temp.getNodes().get(i).incNmrEdges();
						}
					}
					for(int i=0; i<temp.getNodes().size();i++)
					{
						if(temp.getNodes().get(i).getName().equals(n2.getName()))
						{
							e.getNodes().add(temp.getNodes().get(i));
							temp.getNodes().get(i).incNmrEdges();
						}
					}
					
					temp.getEdges().add(e);

				}
				else{

					String node = parts[0];
					String coordinates[] = parts[1].split(",");
					String x = coordinates[0];
					String y = coordinates[1];
					String value = parts[2];

					for(int i=0; i<temp.getNodes().size();i++)
					{
						if(temp.getNodes().get(i).getName().equals(node))
						{
							temp.getNodes().get(i).setValue(Double.parseDouble(value));
							temp.getNodes().get(i).setX(Integer.parseInt(x));
							temp.getNodes().get(i).setY(Integer.parseInt(y));
						}
					}
				}
			}
			reader.close();
			return temp;
		} 
		catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return temp;
		}

	}



	public Graph getWorld() {
		return World;
	}

	public void setWorld(Graph world) {
		World = world;
	}
}
