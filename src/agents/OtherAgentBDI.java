package agents;
import jadex.bdiv3.BDIAgent;
import jadex.bridge.service.RequiredServiceInfo;
import jadex.bridge.service.annotation.Service;
import jadex.bridge.service.search.SServiceProvider;
import jadex.commons.future.DefaultResultListener;
import jadex.micro.annotation.Agent;
import jadex.micro.annotation.AgentBody;

import java.io.IOException;
import java.util.Random;

@Agent
@Service
public class OtherAgentBDI {

	@Agent
	protected BDIAgent OtherAgent;
	public static MapAgent city = new MapAgent();
	public static RadioAgentBDI radio = new RadioAgentBDI();

	public int i=0;

	@AgentBody
	public void body()
	{
		try {
			city.setWorld(city.readGraph("City1"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		for(int j=0; j<100;j++)
		{
			OtherAgent.waitForDelay(2000).get();
			Random r = new Random();
			int value = r.nextInt(50) + 1;

			String node = null;
			boolean encontrou=false;

			while(encontrou==false)
			{
				Random r2 = new Random();
				int n = r2.nextInt(city.getWorld().getNodes().size()) + 1;
				
				if(city.getWorld().getNodes().get(n).getValue()>0)
				{
					node=city.getWorld().getNodes().get(n).getName();
					encontrou=true;
				}
			}
			RadioAgentBDI.lei=value;
			RadioAgentBDI.leiNodeName=node;
		}
	}
}
