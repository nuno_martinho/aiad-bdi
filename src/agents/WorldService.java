package agents;

import jadex.commons.future.IFuture;

public interface WorldService {
  public IFuture<Void> serviceWeather(int value, int e);
}