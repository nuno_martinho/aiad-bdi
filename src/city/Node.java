package city;

import java.util.ArrayList;

public class Node {
	
	private double value;
	private String name;
	private int nmrEdges;
	private int x,y;
	
	private ArrayList<Edge> Edges = new ArrayList<Edge>();
	
	public Node(double v, String n, int xx, int yy) {
		this.value = v;
		this.name = n;
		this.nmrEdges = 0;
		this.x=xx;
		this.y=yy;
	}

	public double getValue() {
		return value;
	}

	public void setValue(double value) {
		this.value = value;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getNmrEdges() {
		return nmrEdges;
	}

	public void setNmrEdges(int nmrEdges) {
		this.nmrEdges = nmrEdges;
	}

	public ArrayList<Edge> getEdges() {
		return Edges;
	}

	public void setEdges(ArrayList<Edge> edges) {
		Edges = edges;
	}
	
	public void incNmrEdges() {
		nmrEdges++;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}
	
}
