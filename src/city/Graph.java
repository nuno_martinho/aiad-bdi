package city;

import java.util.ArrayList;

public class Graph {
	
	private String name;
	
	private ArrayList<Edge> Edges= new ArrayList<Edge>();
	private ArrayList<Node> Nodes = new ArrayList<Node>();
	
	public Graph(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public ArrayList<Edge> getEdges() {
		return Edges;
	}
	
	public void setEdges(ArrayList<Edge> edges) {
		Edges = edges;
	}
	
	public ArrayList<Node> getNodes() {
		return Nodes;
	}
	
	public void setNodes(ArrayList<Node> nodes) {
		Nodes = nodes;
	}
	
	public double getDistanceByNodes(String n1, String n2){
		
		for(int i = 0 ; i < Edges.size() ; i++) {
			if(Edges.get(i).getNodes().get(0).getName().equals(n1) && Edges.get(i).getNodes().get(1).getName().equals(n2))
				return Edges.get(i).getDistance(); 
		}
		
		return 0;
	}
	
}
