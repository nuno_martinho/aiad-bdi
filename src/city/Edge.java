package city;

import java.util.ArrayList;

public class Edge {
	
	private double distance;
	private int ID;
	
	private ArrayList<Node> Nodes= new ArrayList<Node>();
	
	public Edge(double d, int id) {
		this.distance = d;
		this.ID = id;
	}

	public double getDistance() {
		return distance;
	}
	
	public void setDistance(double distance) {
		this.distance = distance;
	}

	public int getID() {
		return ID;
	}

	public void setID(int id) {
		this.ID = id;
	}

	public ArrayList<Node> getNodes() {
		return Nodes;
	}

	public void setNodes(ArrayList<Node> nodes) {
		Nodes = nodes;
	}

	public Object getSource() {
		return this.getNodes().get(0);
	}

	public Node getDestination() {
		return this.getNodes().get(1);
	}
	

}
